/**********************************************************************************
 * $URL: https://source.sakaiproject.org/svn/msgcntr/trunk/messageforums-api/src/java/org/sakaiproject/api/app/messageforums/DefaultPermissionsManager.java $
 * $Id: DefaultPermissionsManager.java 9227 2006-05-15 15:02:42Z cwen@iupui.edu $
 ***********************************************************************************
 *
 * Copyright (c) 2003, 2004, 2005, 2006, 2007, 2008 The Sakai Foundation
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.opensource.org/licenses/ECL-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 **********************************************************************************/
package org.sakaiproject.api.app.messageforums;

/**
 * @author <a href="mailto:rshastri@iupui.edu">Rashmi Shastri</a>
 */
interface DefaultPermissionsManager
{
  String FUNCTION_NEW_FORUM="messagecenter.newForum";
  String FUNCTION_NEW_TOPIC="messagecenter.newTopic";
  String FUNCTION_NEW_RESPONSE="messagecenter.newResponse";
  String FUNCTION_NEW_RESPONSE_TO_RESPONSE="messagecenter.newResponseToResponse";
  String FUNCTION_MOVE_POSTINGS="messagecenter.movePostings";
  String FUNCTION_IDENTIFY_ANON_AUTHORS="messagecenter.identifyAnonAuthors";
  String FUNCTION_CHANGE_SETTINGS="messagecenter.changeSettings";
  String FUNCTION_POST_TO_GRADEBOOK="messagecenter.postToGradebook";
 
  String FUNCTION_READ="messagecenter.read";
  String FUNCTION_REVISE_ANY="messagecenter.reviseAny";
  String FUNCTION_REVISE_OWN="messagecenter.reviseOwn";
  String FUNCTION_DELETE_ANY="messagecenter.deleteAny";
  String FUNCTION_DELETE_OWN="messagecenter.deleteOwn";
  String FUNCTION_MARK_AS_READ="messagecenter.markAsRead";
  
  String MESSAGE_FUNCTION_PREFIX="msg.";
  String MESSAGE_FUNCTION_EMAIL= MESSAGE_FUNCTION_PREFIX +"emailout";
  //unfortunately, emailout was implemented illogically with how realms is supposed to work,
  //so by adding a "permssions extension to the prefix, we can expose the realm permissions w/o exposing
  //emailout
  String MESSAGE_FUNCITON_PREFIX_PERMISSIONS = "permissions.";
  String MESSAGE_FUNCTION_ALLOW_TO_FIELD_GROUPS = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "allowToField.groups";
  String MESSAGE_FUNCTION_ALLOW_TO_FIELD_ALL_PARTICIPANTS = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "allowToField.allParticipants";
  String MESSAGE_FUNCTION_ALLOW_TO_FIELD_ROLES = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "allowToField.roles";
  String MESSAGE_FUNCTION_VIEW_HIDDEN_GROUPS = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "viewHidden.groups";
  String MESSAGE_FUNCTION_ALLOW_TO_FIELD_USERS = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "allowToField.users";
  String MESSAGE_FUNCTION_ALLOW_TO_FIELD_MYGROUPS = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "allowToField.myGroups";
  String MESSAGE_FUNCTION_ALLOW_TO_FIELD_MYGROUPMEMBERS = MESSAGE_FUNCTION_PREFIX + MESSAGE_FUNCITON_PREFIX_PERMISSIONS + "allowToField.myGroupMembers";
  
  
  // control permissions
  boolean isNewForum(String role);

  boolean isNewTopic(String role);

  boolean isNewResponse(String role);

  boolean isResponseToResponse(String role);

  boolean isMovePostings(String role);

  boolean isChangeSettings(String role);

  boolean isPostToGradebook(String role);

  // message permissions
  boolean isRead(String role);

  boolean isReviseAny(String role);

  boolean isReviseOwn(String role);

  boolean isDeleteAny(String role);

  boolean isDeleteOwn(String role);

  boolean isMarkAsRead(String role);

}
